#!/bin/bash

#consider to execute the script with root priv. directly via: 
#curl -sSL https://gitlab.com/jfet/var.scripts/-/raw/main/proxmox-font-changer.sh | bash

# Paths of the files to be modified
file1="/usr/share/javascript/extjs/theme-crisp/resources/theme-crisp-all_1.css"
file2="/usr/share/javascript/extjs/theme-crisp/resources/theme-crisp-all_2.css"
file3="/usr/share/pve-manager/css/ext6-pve.css"

# String to search for in the files
search_string="helvetica, arial, verdana, sans-serif"

# String to replace with

replace_string="arial, roboto, sans-serif"

# Color escape sequences
RD=$(echo "\033[01;31m")
YW=$(echo "\033[33m")
GN=$(echo "\033[1;92m")
CL=$(echo "\033[m")
BFR="\\r\\033[K"
HOLD="-"
CM="${GN}✓${CL}"
CROSS="${RD}✗${CL}"

# Function to replace the string in files
replace_string_in_file() {
    file=$1
    search=$2
    replace=$3

    # Check if the file exists
    if [ -f "$file" ]; then
        # Create a backup copy
        cp "$file" "$file.bakk"
        # Replace the string in the file
        sed -i "s/$search/$replace/g" "$file"
        msg_ok "Changes made to $file. Backup created: $file.bakk"
    else
        msg_error "File $file does not exist."
    fi
}

# Function for green success message
msg_ok() {
    local msg="$1"
    echo -e "${BFR} ${CM} ${GN}${msg}${CM}"
}

# Function for red error message
msg_error() {
    local msg="$1"
    echo -e "${BFR} ${CROSS} ${RD}${msg}${CM}"
}

# Execute the replacement for each file
replace_string_in_file "$file1" "$search_string" "$replace_string"
replace_string_in_file "$file2" "$search_string" "$replace_string"
replace_string_in_file "$file3" "$search_string" "$replace_string"

# Confirmation of changes
if [ $? -eq 0 ]; then
    msg_ok "Changes executed successfully."
else
    msg_error "An error occurred while executing the changes."
fi

# Confirmation of changes
if [ $? -eq 0 ]; then
    msg_ok "Changes executed successfully."

    # Ask if the user wants to restart Proxmox GUI
    read -p "Do you want to restart Proxmox GUI? (y/n): " choice
    case "$choice" in
        y|Y)
            # Restart the pveproxy service
            service pveproxy restart
            msg_ok "Proxmox GUI restarted."
            ;;
        n|N)
            msg_ok "No restart performed."
            ;;
        *)
            msg_error "Invalid choice. No restart performed."
            ;;
    esac
else
    msg_error "An error occurred while executing the changes."
fi

